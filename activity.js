db.users.updateOne([
        {
        "firstName": "Chito" ,
        "lastName": "Miranda",
        "email": "notChito@gmail.com",
        "password": "password1" ,
        "isAdmin": false,
        },
        {
        "firstName": "Vinci" ,
        "lastName": "Montaner",
        "email": "notVinci@gmail.com",
        "password": "password2" ,
        "isAdmin": false,
        },
         {
        "firstName": "Gab Chee" ,
        "lastName": "Kee",
        "email": "notGab@gmail.com",
        "password": "password3" ,
        "isAdmin": false,
        },
         {
        "firstName": "Buwi" ,
        "lastName": "Mendeses",
        "email": "notBuwi@gmail.com",
        "password": "Password4" ,
        "isAdmin": false,
        },
         {
        "firstName": "Darius" ,
        "lastName": "Semaña",
        "email": "notDarius@gmail.com",
        "password": "password5" ,
        "isAdmin": false,
        },
         {
        "firstName": "Dindin" ,
        "lastName": "Moreno",
        "email": "notDin@gmail.com",
        "password": "password6" ,
        "isAdmin": false,
        },
])
db.courses.insertMany([
    {
        "name": "Science",
        "description": "Learn About Earth",
        "price" : 1500,
        "isActive": true,
    },
      {
        "name": "Math",
        "description": "Learn Basic Mathematical Operation",
        "price" : 2500,
        "isActive": true,
    },
      {
        "name": "English",
        "description": "Learn English",
        "price" : 3500,
        "isActive": true,
    }
    ])

db.users.updateOne({"firstName":"Chito"},{$set:{"isAdmin":true}})
db.users.findMany({"isAdmin":false})
db.courses.updateOne({"name":"English"},{$:set:{"isActive":false}})
